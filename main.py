import pandas as panda
import pymongo
import re
import requests
import json

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["moviesdb"]
myRatings = mydb["ratings"]
myMovies = mydb["movies"]
myUser = mydb["users"]

# ratings_cols = ['user_id', 'movie_id', 'rating', 'create']
# ratings = panda.read_csv('/Users/boy-/PycharmProjects/RecommenderMovieLens/ml-100k/u.data', sep='\t',
#                          names=ratings_cols,
#                          usecols=range(4), encoding='latin-1')
# ratings['user_id'] = ratings['user_id'].apply(int)
# ratings['movie_id'] = ratings['movie_id'].apply(int)
# ratings['rating'] = ratings['rating'].apply(int)
# ratings['create'] = ratings['create'].apply(str)
#
#
#

# movies = myMovies.find()
# wtfffff = ""
# for movie in movies:
#     nameMovie = movie['title']
#     if '(' in nameMovie:
#         index = nameMovie.index('(')
#         title = nameMovie[:index - 1]
#         resp = requests.get('http://www.omdbapi.com/?t=' + title + '&apikey=d4fc9d22')
#         if resp.status_code != 200:
#             print("Error" + nameMovie)
#             continue
#         b = resp.json()
#         if 'Poster' in b:
#             poster = b['Poster']
#             movie['poster'] = poster
#         if 'Genre' in b:
#             movie['genre'] = b['Genre']
#         if 'Director' in b:
#             movie['director'] = b['Director']
#         if 'Actors' in b:
#             movie['actors'] = b['Actors']
#         if 'imdbRating' in b:
#             movie['imdbRating'] = b['imdbRating']
#         if 'imdbVotes' in b:
#             movie['imdbVotes'] = b['imdbVotes']
#         myMovies.update({"_id": movie['_id']},
#                         movie)
#         print("Succes", movie["movie_id"])



# for index, row in ratings.iterrows():
#     rating = {"user_id": row["user_id"], "movie_id": row["movie_id"], "rating": row["rating"],
#               "create": row["create"]}
#     x = myRatings.insert_one(rating)
#     print(x.inserted_id)
# print(rating)
# movies_cols = ['movie_id', 'title', 'releaseDate', 'xxx', 'iMDbUrl',
#                'unknown',
#                'action',
#                'adventure',
#                'animation',
#                'childrens',
#                'comedy',
#                'crime',
#                'documentary',
#                'drama',
#                'fantasy',
#                'filmNoir',
#                'horror',
#                'musical',
#                'mystery',
#                'romance',
#                'sciFi',
#                'thriller',
#                'war',
#                'western']
# movies = panda.read_csv('/Users/boy-/PycharmProjects/RecommenderMovieLens/ml-100k/u.item', sep='|', names=movies_cols,
#                         usecols=range(24), encoding='latin-1')
# for index, row in movies.iterrows():
#     movie = {"movie_id": row["movie_id"],
#              "title": row["title"],
#              "releaseDate": row["releaseDate"],
#              "iMDbUrl": row["iMDbUrl"],
#              "unknown": row["unknown"],
#              "action": row["action"],
#              "adventure": row["adventure"],
#              "animation": row["animation"],
#              "childrens": row["childrens"],
#              "comedy": row["comedy"],
#              "crime": row["crime"],
#              "documentary": row["documentary"],
#              "drama": row["drama"],
#              "fantasy": row["fantasy"],
#              "filmNoir": row["filmNoir"],
#              "horror": row["horror"],
#              "musical": row["musical"],
#              "mystery": row["mystery"],
#              "romance": row["romance"],
#              "sciFi": row["sciFi"],
#              "thriller": row["thriller"],
#              "war": row["war"],
#              "western": row["western"]
#              }
#     x = myMovies.insert_one(movie)
#     print(x.inserted_id)

# movies = panda.DataFrame(list(myMovies.find({}, {"_id": 0, "movie_id": 1, "title": 1})))
# ratings = panda.DataFrame(list(myRatings.find({}, {"_id": 0, "user_id": 1, "movie_id": 1, "rating": 1})))
#
# # convert int64 to int for use pandas
# movies['movie_id'] = movies['movie_id'].apply(int)
# ratings['user_id'] = ratings['user_id'].apply(int)
# ratings['movie_id'] = ratings['movie_id'].apply(int)
# ratings['rating'] = ratings['rating'].apply(int)
#
# ratings = panda.merge(movies, ratings)
# userRatings = ratings.pivot_table(index=['user_id'], columns=['title'], values='rating')
# corrMatrix = userRatings.corr()
# corrMatrix = userRatings.corr(method='pearson', min_periods=50)


# for x in range(1, 943):
# myRatings = userRatings.loc[943].dropna()
# simCandidates = panda.Series()
# for i in range(0, len(myRatings.index)):
#     print("adding sims for " + myRatings.index[i] + "...")
#     # Retrieve similar movies to this one that I rated
#     sims = corrMatrix[myRatings.index[i]].dropna()
#     # Now scale its similarity by how well I rated this movie
#     sims = sims.map(lambda x: x * myRatings[i])
#     # Add the score to the list of similarity candidates
#     simCandidates = simCandidates.append(sims)
# print('sorting in decreasing order of similarity score: ')
# simCandidates.sort_values(inplace=True, ascending=False)
# print(simCandidates.head(10))
# simCandidates = simCandidates.groupby(simCandidates.index).sum()
# print("Adding up the similarity scores of duplicate values:")
# simCandidates.sort_values(inplace=True, ascending=False)
# print(simCandidates.head(10))
# print("Filtering the result to remove already rated movies:")
# filteredSims = simCandidates.drop(myRatings.index, errors='ignore').reset_index()
# # print(filteredSims.head(50))
# for index, row in filteredSims.iterrows():
#     # movie = {"movie_id": row["movie_id"], "title": row["title"]}
#     nameMovie = row["index"]
#     movie = myMovies.find_one({"title": nameMovie})
#     print(movie)
#     if index == 50:
#         break
