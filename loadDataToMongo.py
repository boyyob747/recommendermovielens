import pandas as panda
import pymongo
import re
import requests
import json

# init mongodb
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["moviesdb"]
myRatings = mydb["ratings"]
myMovies = mydb["movies"]
myUser = mydb["users"]

# load user from u.user then save to mongo
user_cols = ['user_id', 'age', 'gender', 'occupation', 'zipCode']
users = panda.read_csv('/Users/boy-/PycharmProjects/RecommenderMovieLens/ml-100k/u.user', sep='|', names=user_cols,
                       usecols=range(5), encoding='latin-1')
for index, row in users.iterrows():
    user = {"user_id": row["user_id"], "age": row["age"], "gender": row["gender"], "occupation": row["occupation"],
            "zipCode": row["zipCode"], "password": "password" + str(index), "username": "username" + str(index)}
    x = myUser.insert_one(user)
    print(x.inserted_id, "Save user success", index)

# load movie from u.item then save to mongo
movies_cols = ['movie_id', 'title', 'releaseDate', 'xxx', 'iMDbUrl',
               'unknown',
               'action',
               'adventure',
               'animation',
               'childrens',
               'comedy',
               'crime',
               'documentary',
               'drama',
               'fantasy',
               'filmNoir',
               'horror',
               'musical',
               'mystery',
               'romance',
               'sciFi',
               'thriller',
               'war',
               'western']
movies = panda.read_csv('/Users/boy-/PycharmProjects/RecommenderMovieLens/ml-100k/u.item', sep='|', names=movies_cols,
                        usecols=range(24), encoding='latin-1')
for index, row in movies.iterrows():
    movie = {"movie_id": row["movie_id"],
             "title": row["title"],
             "releaseDate": row["releaseDate"],
             "iMDbUrl": row["iMDbUrl"],
             "unknown": row["unknown"],
             "action": row["action"],
             "adventure": row["adventure"],
             "animation": row["animation"],
             "childrens": row["childrens"],
             "comedy": row["comedy"],
             "crime": row["crime"],
             "documentary": row["documentary"],
             "drama": row["drama"],
             "fantasy": row["fantasy"],
             "filmNoir": row["filmNoir"],
             "horror": row["horror"],
             "musical": row["musical"],
             "mystery": row["mystery"],
             "romance": row["romance"],
             "sciFi": row["sciFi"],
             "thriller": row["thriller"],
             "war": row["war"],
             "western": row["western"]
             }
    x = myMovies.insert_one(movie)
    print(x.inserted_id, "Save movie success", index)

# load ratings from u.data then save to mongo
ratings_cols = ['user_id', 'movie_id', 'rating', 'create']
ratings = panda.read_csv('/Users/boy-/PycharmProjects/RecommenderMovieLens/ml-100k/u.data', sep='\t',
                         names=ratings_cols,
                         usecols=range(4), encoding='latin-1')
ratings['user_id'] = ratings['user_id'].apply(int)
ratings['movie_id'] = ratings['movie_id'].apply(int)
ratings['rating'] = ratings['rating'].apply(int)
ratings['create'] = ratings['create'].apply(str)
for index, row in ratings.iterrows():
    rating = {"user_id": row["user_id"], "movie_id": row["movie_id"], "rating": row["rating"],
              "create": row["create"]}
    x = myRatings.insert_one(rating)
    print(x.inserted_id, "Save rating success", index)
