from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask.json import jsonify
import pymongo


app = Flask(__name__)
api = Api(app)


class Movies(Resource):
    def get(self):
        myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        mydb = myclient["moviesdb"]
        myRatings = mydb["ratings"]
        myMovie = mydb["movies"]
        output = []
        for s in myMovie.find():
            output.append({'_id': s['_id'], 'movie_id': s['movie_id'], 'title': s['title']})
        return jsonify({'result': output})


api.add_resource(Movies, '/movies')  # Route_1
if __name__ == '__main__':
    app.run(port='5003')
