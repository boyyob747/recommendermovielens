from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo
from pymongo import MongoClient
import pymongo
import pandas as panda
import json
from bson.json_util import dumps
import time

app = Flask(__name__)

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["moviesdb"]
myRatings = mydb["ratings"]
myMovies = mydb["movies"]
myUser = mydb["users"]


@app.route("/get_recommender/<user_id>", methods=['GET'])
def get_recommender(user_id):
    try:
        myRatings = mydb["ratings"]
        myMovies = mydb["movies"]
        checkUser = myUser.count({"user_id": int(user_id)})
        if checkUser <= 0:
            return jsonify({'error': "No user"})
        checkRate = myRatings.count({"user_id": int(user_id)})
        if checkRate <= 0:
            return jsonify({'error': "Ban chua rate phim nao\nHay rate it nhat 1 phim"})

        movies = panda.DataFrame(list(myMovies.find({}, {"_id": 0, "movie_id": 1, "title": 1})))
        ratings = panda.DataFrame(list(myRatings.find({}, {"_id": 0, "user_id": 1, "movie_id": 1, "rating": 1})))
        #
        # # convert int64 to int for use pandas
        movies['movie_id'] = movies['movie_id'].apply(int)
        ratings['user_id'] = ratings['user_id'].apply(int)
        ratings['movie_id'] = ratings['movie_id'].apply(int)
        ratings['rating'] = ratings['rating'].apply(int)
        ratings = panda.merge(movies, ratings)

        userRatings = ratings.pivot_table(index=['user_id'], columns=['title'], values='rating')
        corrMatrix = userRatings.corr()
        corrMatrix = userRatings.corr(method='pearson', min_periods=50)

        myRatings = userRatings.loc[int(user_id)].dropna()
        simCandidates = panda.Series()
        for i in range(0, len(myRatings.index)):
            print("adding sims for " + myRatings.index[i] + "...")
            # Retrieve similar movies to this one that I rated
            sims = corrMatrix[myRatings.index[i]].dropna()
            # Now scale its similarity by how well I rated this movie
            sims = sims.map(lambda x: x * myRatings[i])
            # Add the score to the list of similarity candidates
            simCandidates = simCandidates.append(sims)
        simCandidates.sort_values(inplace=True, ascending=False)
        simCandidates = simCandidates.groupby(simCandidates.index).sum()
        simCandidates.sort_values(inplace=True, ascending=False)
        filteredSims = simCandidates.drop(myRatings.index, errors='ignore').reset_index()
        # print(filteredSims.head(50))
        output = []
        for index, row in filteredSims.iterrows():
            nameMovie = row["index"]
            movie = myMovies.find_one({"title": nameMovie})
            movie.pop("_id")
            output.append(movie)
            if index == 50:
                break
        return jsonify(output)
    except Exception as e:
        return jsonify({'error': str(e)})


@app.route("/get_all_movies", methods=['GET'])
def get_all_movies():
    try:
        movies = myMovies.find()
        output = []
        for x in movies:
            x.pop('_id')
            output.append(x)
        return jsonify({'result': output})
    except Exception as e:
        return jsonify({'error': str(e)})


@app.route("/get_user/<username>", methods=['GET'])
def get_user(username):
    try:
        user = myUser.find_one({"username": username})
        if user is not None:
            user.pop('_id')
            return jsonify(user)
        else:
            return jsonify({'error': "No user"})
    except Exception as e:
        return jsonify({'error': str(e)})


@app.route("/get_list_title_top_moview", methods=['GET'])
def get_list_title_top_moview():
    try:
        url = "https://www.moviequotesandmore.com/wp-content/uploads/afi-top-100.jpg"
        output = [{"title": "Top Rated Movies", "key_get": "get_top_ratings", "poster": url},
                  {"title": "Popular Comedy", "key_get": "get_top_movies_by_type/comedy", "poster": url},
                  {"title": "Popular Sci-Fi", "key_get": "get_top_movies_by_type/sciFi", "poster": url},
                  {"title": "Popular Drama", "key_get": "get_top_movies_by_type/drama", "poster": url},
                  {"title": "Popular War", "key_get": "get_top_movies_by_type/war", "poster": url},
                  {"title": "Popular Thriller", "key_get": "get_top_movies_by_type/thriller", "poster": url},
                  {"title": "Popular Romance", "key_get": "get_top_movies_by_type/romance", "poster": url}]
        return jsonify(output)
    except Exception as e:
        return jsonify({'error': str(e)})


@app.route("/get_top_ratings/<user_id>", methods=['GET'])
def get_top_ratings(user_id):
    try:
        all_movies = panda.DataFrame(list(myMovies.find({}, {"_id": 0, "movie_id": 1, "title": 1})))
        all_ratings = panda.DataFrame(list(myRatings.find({}, {"_id": 0, "user_id": 1, "movie_id": 1, "rating": 1})))

        all_movies['movie_id'] = all_movies['movie_id'].apply(int)
        all_ratings['user_id'] = all_ratings['user_id'].apply(int)
        all_ratings['movie_id'] = all_ratings['movie_id'].apply(int)
        all_ratings['rating'] = all_ratings['rating'].apply(int)
        all_ratings = panda.merge(all_movies, all_ratings)

        most_rated = all_ratings.groupby('title').size().sort_values(ascending=False)[:50].reset_index()
        output = []
        for index, row in most_rated.iterrows():
            nameMovie = row["title"]
            movie = myMovies.find_one({"title": nameMovie})
            rating = myRatings.find_one({"movie_id": int(movie["movie_id"]), "user_id": int(user_id)})
            if rating is not None:
                movie['my_rate'] = rating['rating']
            movie.pop("_id")
            output.append(movie)
            if index == 50:
                break
        return jsonify(output)
    except Exception as e:
        return jsonify({'error': str(e)})


@app.route("/get_top_movies_by_type/<type_movie>/<user_id>", methods=['GET'])
def get_top_movies_by_type(type_movie, user_id):
    try:
        all_movies_drama = panda.DataFrame(
            list(myMovies.find({str(type_movie): 1}, {"_id": 0, "movie_id": 1, "title": 1})))
        all_ratings = panda.DataFrame(list(myRatings.find({}, {"_id": 0, "user_id": 1, "movie_id": 1, "rating": 1})))

        all_movies_drama['movie_id'] = all_movies_drama['movie_id'].apply(int)
        all_ratings['user_id'] = all_ratings['user_id'].apply(int)
        all_ratings['movie_id'] = all_ratings['movie_id'].apply(int)
        all_ratings['rating'] = all_ratings['rating'].apply(int)
        all_ratings = panda.merge(all_movies_drama, all_ratings)

        most_rated = all_ratings.groupby('title').size().sort_values(ascending=False)[:50].reset_index()
        print(most_rated)
        output = []
        for index, row in most_rated.iterrows():
            nameMovie = row["title"]
            movie = myMovies.find_one({"title": nameMovie})
            rating = myRatings.find_one({"movie_id": int(movie["movie_id"]), "user_id": int(user_id)})
            if rating is not None:
                movie['my_rate'] = rating['rating']
            movie.pop("_id")
            output.append(movie)
            if index == 50:
                break
        return jsonify(output)
    except Exception as e:
        return jsonify({'error': str(e)})


@app.route('/register', methods=['POST'])
def register():
    try:
        data = json.loads(request.data)
        user_id = myUser.count() + 1
        age = '24'
        gender = 'M'
        occupation = 'student'
        zipCode = '99999'
        username = data['username']
        password = data['password']
        checkUsername = myUser.count({'username': username})
        if checkUsername > 0:
            return dumps({'success': False, 'message': 'Username da co tren server roi!'})
        else:
            user = {
                'user_id': user_id,
                'age': age,
                'gender': gender,
                'occupation': occupation,
                'zipCode': zipCode,
                'username': username,
                'password': password
            }
            myUser.insert_one(user)
            return dumps({'success': True, 'message': 'Register Thanh cong!'})
    except Exception as e:
        return dumps({'success': False, 'message': str(e)})


@app.route('/login', methods=['POST'])
def login():
    try:
        data = json.loads(request.data)
        username = data['username']
        password = data['password']
        checkUsername = myUser.count({'username': username, 'password': password})
        if checkUsername > 0:
            return dumps({'success': True, 'message': 'Dang nhap thanh cong!'})
        else:
            return dumps({'success': False, 'message': 'Username hoac password sai!'})
    except Exception as e:
        return dumps({'success': False, 'message': str(e)})


@app.route('/rate_movie', methods=['POST'])
def rate_movie():
    try:
        ts = int(time.time())
        data = json.loads(request.data)
        user_id = data['user_id']
        movie_id = data['movie_id']
        rating = data['rating']
        create = str(ts)
        checkIsRated = myRatings.count({'user_id': str(user_id), 'movie_id': str(movie_id)})
        if checkIsRated > 0:
            rate = myRatings.find_one({'user_id': user_id, 'movie_id': movie_id})
            rate['rating'] = rating
            print(rate['_id'])
            myRatings.update({"_id": rate['_id']},
                             rate)
            return dumps({'success': True, 'message': 'Ban update rate movie thanh cong!'})
        else:
            rating = {
                "user_id": user_id,
                "movie_id": movie_id,
                "rating": rating,
                "create": create
            }
            myRatings.insert_one(rating)
            return dumps({'success': True, 'message': "Da rate movie thanh cong!"})
    except Exception as e:
        return dumps({'success': False, 'message': str(e)})


if __name__ == '__main__':
    app.run(port='3000')
