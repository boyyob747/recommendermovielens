import pandas as panda
import pymongo
import re
import requests
import json
from google_images_download import google_images_download   #importing the library
response = google_images_download.googleimagesdownload()   #class instantiation

# init mongodb
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["moviesdb"]
myRatings = mydb["ratings"]
myMovies = mydb["movies"]
myUser = mydb["users"]
from imagesoup import ImageSoup

movie = myMovies.find_one({"movie_id": 164})
print(movie)
nameMovie = movie['title']
soup = ImageSoup()
images = soup.search(nameMovie, n_images=10)
if len(images) > 0:
    url = images[0].URL
    movie['poster'] = url
    myMovies.update({"_id": movie['_id']},
                            movie)
    print("Succes", movie)
# i = 0
# for movie in movies:
#     nameMovie = movie['title']
#     if '(' in nameMovie:
#         # index = nameMovie.index('(')
#         # title = nameMovie[:index - 1]
#         # print(nameMovie)
#         soup = ImageSoup()
#         images = soup.search(nameMovie, n_images=10)
#         if len(images) > 0:
#             url = images[0].URL
#             movie['poster'] = url
#             myMovies.update({"_id": movie['_id']},
#                             movie)
#             print("Succes", movie)
#         else:
#             continue
#         i += 1
# print("Total : ",i)
